# reference: https://github.com/firefly-cpp/alpine-container-data-science
FROM alpine:edge

ENV NAME=succulent VERSION=0 ARCH=x86_64

# Define the environment variable for Gunicorn workers, per documentation 2-4 workers per core
# https://docs.gunicorn.org/en/stable/run.html#commonly-used-arguments
ENV GUNICORN_WORKERS=2

WORKDIR /succulent-app

ENV PACKAGES="\
    py3-succulent \
    py3-gunicorn \
"

RUN echo 'https://dl-cdn.alpinelinux.org/alpine/edge/testing' >> /etc/apk/repositories \
    && apk update \
    && apk upgrade && apk add --no-cache \
    python3 \
    $PACKAGES \
    && rm -rf /var/cache/apk/*

COPY run.py .
COPY configuration.yml .

EXPOSE 8080


CMD gunicorn -b 0.0.0.0:8080 -w ${GUNICORN_WORKERS} -t 120 run:app
